# Vagrant k8s multinode cluster

## Overview
	- The repository consists of a Vagrant script that provisions one master node and two worker nodes running on centos7 boxes and hosts the Kubernetes cluster that the dockerized Golang app is deployed to. 

	- Commits and push changes to the repository trigger the configured GitlabCI pipeline defined in the .gitlab-ci-yml automating the deployment of the app to the Kubernetes cluster.																


## Getting Started

As shown below, you may simply clone the Gitlab repo to your local machine.

```shell
$ git clone https://gitlab.com/ronnie420/vagrant-k8s-multinode-cluster.git
```

## Quickstart

### Prerequisites

* [vagrant](https://www.vagrantup.com/downloads.html)
* [virtualbox](https://www.virtualbox.org/wiki/Downloads)
* [docker](https://docs.docker.com/engine/install/)
* [gitlab-runner](https://docs.gitlab.com/runner/install/)

* When you're in your repository, use the left menu to open up the Settings -> CI / CD -> Environment variables. We're going 	to add the following configuration:

    DOCKER_USER. This is the Docker user you use to login to the Docker Hub.
    DOCKER_PASSWORD. This is the Docker passwrod you use to login to the Docker Hub.

*  Obtain a token for a shared or specific Runner via GitLab’s interface as shown here https://docs.gitlab.com/ee/ci/runners/
	The runner will be the one running the pipeline jobs as defined in the gitlab-ci.yml file in the repo.

* Register the Gitlab-runner
```shell
$ sudo gitlab-runner register
```

* Enter Gitlab Instance URL
```shell
$ Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com )
```

* Enter the tags associated with the Runner,
```shell
$ Enter the gitlab-ci tags for this runner (comma separated):
```

* Enter the executor: in this case it'll be shell since the app is deployed from a local env
```shell
$ Enter the gitlab-ci tags for this runner (comma separated):
```


## Make sure git is installed

Instal [git](https://git-scm.com/downloads) if you don't already have it.

## Open a shell and clone

```bash
$ git clone https://gitlab.com/ronnie420/vagrant-k8s-multinode-cluster.git
$ cd vagrant-k8s-multinode-cluster.
```

## Starting the cluster

You can create the cluster with:

```bash
$ vagrant up
```

## Deploy the application

* Apply the deployment Kubernetes manifest to provision the pods.
  ```sh
  $ vagrant ssh master -c 'kubectl apply -f /vagrant/k8s/deployment.yaml'
  ```

* Apply the service Kubernetes manifest to expose the service outside the cluster
  ```sh
  $ vagrant ssh master -c 'kubectl apply -f /vagrant/k8s/service.yaml'
  ```


## Clean up

You can delete the cluster with:

```bash
$ vagrant destroy -f
```


* Application should be accessed on [golang-hello-world](http://localhost:8080/)

